### Mise à jour des sources listes
echo "" > /etc/apt/sources.list
add-apt-repository "deb [arch=amd64] http://deb.debian.org/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) main contrib non-free"
add-apt-repository "deb [arch=amd64] http://deb.debian.org/$(. /etc/os-release; echo "$ID") $(lsb_release -cs)-updates main contrib non-free"
add-apt-repository "deb [arch=amd64] http://security.debian.org/$(. /etc/os-release; echo "$ID") $(lsb_release -cs)/updates main contrib non-free"

### Mise à jour du système
apt update -y && apt upgrade -y

### Installation des dépendances requises
apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y
### Ajout du dépôt
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"

### Installation de docker-ce
apt update -y
apt install docker-ce -y

### Lancement du premier container
docker run hello-world
